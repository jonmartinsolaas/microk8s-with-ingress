# Ubuntu microk8s with ingress

Even though it is not recommended to run microk8s, minikube or some such for 
production there are still situations where you'd want to expose apps running
in microk8s to the world, for instance if the site simply doesn't need all
scaling and failover in the Kubernetes platform, but you still want to stay
on that platform instead of just running on docker for that particular app. 
Hobby projects and learning projects would be examples.

I previously used Charmed Kubernetes set up with JuJu on a single host with a 
bunch of LXD containers, just to get close to the "real thing", but I figured
it was just a lot of extra complexity for no reason, so hence microk8s instead.

The following is based on 
https://medium.com/@robertdiebels/guide-running-microk8s-nginx-ingress-on-centos-7-18d767e6472a
and adapted for Ubuntu instead of CentOS 7.

# Setting it up

## Prerequisites

I used 18.04 and I bet 20.04 will work as well, I'm planning to upgrade when
available.

## Steps
`sudo apt-get install snap snapd`

`sudo snap install microk8s --channel=1.18 --classic`

`sudo microk8s.status --wait-ready`

`sudo snap alias microk8s.kubectl kubectl`

After this you probably need to log out and in again.

`sudo microk8s.enable ingress`

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml`

Content of mandatory.yaml in it's current version is included in this repo.






